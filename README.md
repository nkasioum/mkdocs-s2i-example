# MkDocs S2I

* [How to Create an S2I Builder Image](https://blog.openshift.com/create-s2i-builder-image/)
* [Documentation / OpenShift Container Platform 3.11 / Developer Guide / Builds / Build Inputs](https://docs.openshift.com/container-platform/3.11/dev_guide/builds/build_inputs.html) (choices of source input to the builder image - usually a git repository, but could it be an EOS folder?)
* [Documentation / OpenShift Container Platform 3.11 / Creating Images/ Image Metadata](https://docs.openshift.com/container-platform/3.11/creating_images/metadata.html) (what metadata should we set in the builder image to make it usable)
* [OpenShift base images (core variant) - centos/s2i-core-centos7](https://github.com/sclorg/s2i-base-container/tree/master/core) (core image for all standard S2I images)
* [OpenShift base images - centos/s2i-base-centos7]((https://github.com/sclorg/s2i-base-container/tree/master/core) (base image for all standard S2I images)
* [Nginx container images - centos/nginx-1.14-centos7](https://github.com/sclorg/nginx-container)
