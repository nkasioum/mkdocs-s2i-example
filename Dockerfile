FROM python:3.7-alpine
LABEL maintainer="CERN Authoring <authoring@cern.ch>" \
      io.k8s.description="MkDocs S2I" \
      io.k8s.display-name="MkDocs S2I" \
      io.openshift.tags="MkDocs,S2I" \
      io.openshift.expose-services="8000:http" \
      io.openshift.s2i.scripts-url="image:///s2i"
RUN pip install mkdocs mkdocs-material
WORKDIR /opt
COPY ./s2i/bin/ /s2i
EXPOSE 8000
CMD ["/s2i/usage"]
